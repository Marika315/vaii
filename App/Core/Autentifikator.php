<?php

namespace App\Core;

class Autentifikator
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    function JeToAdmin() {
        $admin = "admin@gmail.com";
        if (($this->JeNiektoPrihlaseny() == 1) && ($admin == $this->DajPrihlaseneho()->getEmail())) {
            return 1;
        } else {
            return 0;
        }
    }

    function JeNiektoPrihlaseny() {
        if (isset($_SESSION["user"])) {
            return 1;
        } else {
            return 0;
        }
}

    function DajPrihlaseneho() {
        return $_SESSION["user"];
    }

    function Prihlasenie($kto) {
        $_SESSION["user"] = $kto;
    }

    function Odhlasenie() {
        if (isset($_SESSION["user"])) {
            unset($_SESSION["user"]);
            session_destroy();
        }
    }
}

