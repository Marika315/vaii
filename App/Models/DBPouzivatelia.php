<?php

namespace App\Models;
use PDO;

class DBPouzivatelia
{
    /**
     * @var PDO
     */

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'pohybovecentrum';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
    }

    function nacitajVsetko()
    {
        $pouzivatelia = [];
        $dbPouzivatelia = $this->db->query('SELECT * FROM users');
        foreach ($dbPouzivatelia as $pouzivatel)
        {
            $pouzivatel2 = new Pouzivatel($pouzivatel['meno'], $pouzivatel['priezvisko'],$pouzivatel['mail'], $pouzivatel['heslo']);
            $pouzivatel2->setId($pouzivatel['id']);
            $pouzivatelia[] = $pouzivatel2;
        }
        return $pouzivatelia;
    }

    function UlozKlienta(Pouzivatel $pouzivatel) {
        $meno22=$pouzivatel->getMeno();
        $priezvisko22=$pouzivatel->getPriezvisko();
        $mail22=$pouzivatel->getEmail();
        $heslo22=password_hash($pouzivatel->getHeslo(),PASSWORD_DEFAULT);
        $this->db->query("INSERT INTO users(meno,priezvisko,mail,heslo) VALUES ('$meno22','$priezvisko22','$mail22','$heslo22')");
    }

    function VymazKlienta($id) {
        $stmt = $this->db->prepare('DELETE FROM users where id = ? ' );
        $stmt->execute([$id]);
    }

    function UpravUdaje($id, $newMeno, $newPriezvisko, $newEmail, $newHeslo) {
        $statement = $this->db->query("SELECT COUNT(mail) FROM users WHERE mail = '$newEmail'");
        $result = $statement->fetch();
        $oldMail = $this->db->query("SELECT email FROM uzivatelia WHERE id = id");
        if ($result[0] == 0 || $oldMail == $newEmail) {
            $this->db->query("UPDATE uzivatelia SET meno = '$newMeno', priezvisko = '$newPriezvisko', email = '$newEmail', heslo = '$newHeslo' WHERE id = '$id'");
            return 1;
        } else {
            return 0;
        }

    }
    function JeToUzivatel($email, $heslo)
    {
        $statement = $this->db->query("SELECT heslo FROM users WHERE mail = '$email'");
        $result = $statement->fetch();
            if (($result[0] != "") && password_verify($heslo,$result[0])) {
                return 1;
            } else {
                return 0;
            }
        }

    function DajUzivatela($mail) {
        $statement = $this->db->query("SELECT * FROM users WHERE mail = '$mail'");
        $result = $statement->fetch();
        return new Pouzivatel($result["meno"],$result["priezvisko"],$result["mail"],$result["heslo"],$result["id"]);
    }


}