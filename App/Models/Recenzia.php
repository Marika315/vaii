<?php

namespace App\Models;

class Recenzia
{
    private $meno;
    private $text;
    private $id;
    public function __construct($meno1, $text1)
    {
        $this->meno = $meno1;
        $this->text = $text1;
    }

    /**
     * @return mixed
     */
   public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMeno()
    {
        return $this->meno;
    }

    /**
     * @param mixed $meno
     */
    public function setMeno($meno): void
    {
        $this->meno = $meno;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

}
