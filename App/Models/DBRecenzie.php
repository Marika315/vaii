<?php

namespace App\Models;

use PDO;

class DBRecenzie
{
    /**
     * @var PDO
     */

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'pohybovecentrum';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
    }

    function nacitajVsetko()
    {
        $recenzie = [];
        $dbRecenzie = $this->db->query('SELECT * FROM recenzia');
        foreach ($dbRecenzie as $recenzia)
        {
            $recenzia2 = new Recenzia($recenzia['meno'], $recenzia['text']);
            $recenzia2->setId($recenzia['id']);
            $recenzie[] = $recenzia2;
        }
        return $recenzie;
    }

    function Uloz(Recenzia $recenzia) {
        $meno2=$recenzia->getMeno();
        $text2=$recenzia->getText();
        $this->db->query("INSERT INTO recenzia(meno, text) VALUES ('$meno2','$text2')");
    }

    function Vymaz($id) {
        $stmt = $this->db->prepare('DELETE FROM recenzia where id = ? ' );
        $stmt->execute([$id]);
    }

    function Uprav($id, $newName, $newText) {
        $stmt = $this->db->prepare("UPDATE recenzia SET meno=?, text=? where id=?");
        $stmt->execute([$newName,$newText,$id]);
    }
}
