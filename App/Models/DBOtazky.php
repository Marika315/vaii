<?php

namespace App\Models;

use PDO;

class DBOtazky
{
    /**
     * @var PDO
     */

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'pohybovecentrum';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
    }

    function nacitajVsetkyOtazky()
    {
        $otazky = [];
        $dbOtazky = $this->db->query('SELECT * FROM otazky');
        foreach ($dbOtazky as $otazka)
        {
            $otazka2 = new Otazka($otazka['text']);
            $otazka2->setId($otazka['id']);
            $otazky[] = $otazka2;
        }
        return $otazky;
    }

    function UlozOtazku(Otazka $otazka) {
        $otazka2=$otazka->getText();
        $this->db->query("INSERT INTO otazky(text) VALUES ('$otazka2')");
    }

    function VymazOtazku($id) {
        $stmt = $this->db->prepare('DELETE FROM otazky where id = ? ' );
        $stmt->execute([$id]);
    }
}