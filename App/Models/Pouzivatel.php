<?php

namespace App\Models;

class Pouzivatel
{
    private $id;
    private $meno;
    private $priezvisko;
    private $email;
    private $heslo;

    /**
     * Pouzivatel constructor.
     * @param $id
     * @param $meno
     * @param $priezvisko
     * @param $email
     * @param $heslo
     */
    public function __construct($meno, $priezvisko, $email, $heslo, $id=0)
    {
        $this->id = $id;
        $this->meno = $meno;
        $this->priezvisko = $priezvisko;
        $this->email = $email;
        $this->heslo = $heslo;
    }

    /**
     * @return mixed
     */
    public function getMeno()
    {
        return $this->meno;
    }

    /**
     * @return mixed
     */
    public function getPriezvisko()
    {
        return $this->priezvisko;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getHeslo()
    {
        return $this->heslo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}