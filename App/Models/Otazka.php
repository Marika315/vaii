<?php

namespace App\Models;

class Otazka
{
    private $id;
    private $textOtazky;


    /**
     * Otazka constructor.
     * @param $id
     * @param $textOtazky
     */
    public function __construct($textOtazky, $id = 0)
    {
        $this->id = $id;
        $this->textOtazky = $textOtazky;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->textOtazky;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
}
