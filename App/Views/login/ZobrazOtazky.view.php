<?php /** @var Array $data */ ?>

<div class="container">
    <h2 id="nazov_pouzivatelia">Otázky</h2>
    <table class="table table-dark table-striped" id= "tableOtazky" >
        <thead>
        <tr>
            <th>ID</th>
            <th>OTÁZKA</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var Otazka $otazka1 */
            foreach ($data['otazky'] as $otazka1) { ?>
        <tr>
             <th><?=$otazka1->getId()?></th>
            <td><?=$otazka1->getText()?></td>
            <td><a href="?c=login&a=zmazOtazku&id=<?=$otazka1->getId()?>" class="btn btn-warning ">Zmaž</a></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
