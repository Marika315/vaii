<div class="container col-md-12 align-content-center">
    <div class="container mt-3">
        <h2 id="nazov_recenzia1">Registruj sa </h2>
        <p id="nazov_recenzia2">Vyplnením tohto formulára sa môžeš zaradiť medzi klientov nášho Pohybového centra.</p>
        <div id="oknoReg">
            <form>
                <div class="form-row col-md-12 ">
                    <div class="form-group col-md-3">
                        <label for="inputAddress">Meno</label>
                        <input type="text" class="form-control" id="inputAddress" name="meno1">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputAddress2">Priezvisko</label>
                        <input type="text" class="form-control" id="inputAddress2" name="priezvisko1">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" name="email1">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputPassword4">Heslo</label>
                    <input type="password" class="form-control" id="inputPassword4" name="heslo1">
                </div>
                <button formmethod="post" type="submit" class="btn btn-dark">Registrovať sa</button>
            </form>
        </div>
    </div>
</div>


