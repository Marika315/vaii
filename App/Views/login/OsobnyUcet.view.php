<?php /** @var Array $data */
/** @var \App\Core\Autentifikator $auten */
$auten = new \App\Core\Autentifikator();
?>
<?php if ($auten->JeToAdmin() == 1) { ?>
    <div class="container">
        <div id="nazov_pouzivatelia">
            <h2>Používatelia</h2>
        </div>
        <table id= "tableUsers" class="table  table-dark  table-striped ">
            <thead>
            <tr>
                <th>ID</th>
                <th>MENO</th>
                <th>PRIEZVISKO</th>
                <th>EMAIL</th>
                <th>HESLO</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
    <?php
} else { ?>
<div id="OsobneUdaje">
    <h1>Vitajte <?= $auten->DajPrihlaseneho()->getMeno() ?> ! </h1>
    <h3>Vaše osobné údaje sú nasledovné!</h3>
</div>
<div class="container">
    <table class="table table-dark table-striped " >
        <thead>
        <tr>
            <th>ID</th>
            <th>MENO</th>
            <th>PRIEZVISKO</th>
            <th>EMAIL</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th><?= $auten->DajPrihlaseneho()->getId() ?></th>
            <td><?= $auten->DajPrihlaseneho()->getMeno() ?></td>
            <td><?= $auten->DajPrihlaseneho()->getPriezvisko() ?></td>
            <td><?= $auten->DajPrihlaseneho()->getEmail() ?></td>
        </tr>
        </tbody>
    </table>
</div>
<?php } ?>


