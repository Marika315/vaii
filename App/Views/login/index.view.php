<?php /** @var Array $data */ ?>

<div class="container col-md-12 align-content-center">
    <div class="container mt-3">
        <h2 id="nazov_Prihlasenie">Prihlásenie</h2>
        <div id="prihlasovaciFormular" class="container mt-5">
            <form class="form-horizontal  p-3">
                <div class="form-group col-md-12">
                    <label class="control-label col-sm-2" for="email"> <strong> Email: </strong> </label>
                    <div class="col-sm-20">
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-sm-2" for="password"> <strong> Heslo: </strong> </label>
                    <div class="col-sm-20">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button formmethod="post" type="submit" name="submit" class="btn btn-dark">Potvrdiť</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="<?= $data['clas'] ?>" role="<?= $data['role'] ?>">
            <?= $data['message'] ?>
        </div>
    </div>
</div>

