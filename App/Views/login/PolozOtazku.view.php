
<?php /** @var Array $data */ ?>

<div class="container mt-3">
    <h2>Chceš nám položiť nejakú otázku? </h2>
    <p>Opýtaj sa nás čokoľvek,či už ohľadom ponúkaných služieb alebo problémov s tvojím osobným účtom. </p>
    <form>
        <div class="form-group ">
            <label for="comment"> <strong>OTÁZKA:</strong></label>
            <textarea class="form-control" rows="5" id="comment" name="text3"></textarea>
        </div>
        <button formmethod="post" type="submit" class="btn btn-warning">Odoslať</button>
        <div class="<?=$data['clas']?>" role="<?=$data['role']?>">
            <?=$data['message']?>
        </div>
    </form>
</div>

