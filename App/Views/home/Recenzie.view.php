<?php
/** @var Array $data */
?>

<div class="container col-md-10 align-content-center">
    <div class="container mt-3">
        <h2 id="nazov_recenzia1">Recenzie</h2>
        <p id="nazov_recenzia2">Vyjadrite svoje skúsenosti s naším pohybovým centrom.</p>
        <a href="?c=home&a=pridajRecenziu" class="btn btn-dark"> + Pridaj Recenziu </a>

        <?php /** @var /App/Models/Recenzia $recenzia */
        foreach ($data['recenzia']as $recenzia) { ?>
        <div>
            <div class="media border p-3 bg-warning">
                <img src="vaii/Images/iconC.png" alt="<?= $recenzia->getMeno() ?>" class="mr-3 mt-3 rounded-circle"
                     style="width:60px;">
                <div class="media-body">
                    <h4><?= $recenzia->getMeno() ?> </h4>
                    <p><?= $recenzia->getText() ?> </p>
                    <div>
                        <div>
                            <a href="?c=home&a=upravRecenziu&id=<?= $recenzia->getId() ?>&stareMeno=<?= $recenzia->getMeno() ?>&staryText=<?= $recenzia->getText() ?>"
                               class="btn btn-dark">Uprav</a>
                            <a href="?c=home&a=zmazRecenziu&id=<?= $recenzia->getId() ?>" class="btn btn-dark">Zmaž</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <?php } ?>

    </div>
</div>


