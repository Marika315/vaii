<div>
    <div class="container mt-3">
        <div id="xxx" class="row">
            <div class="col-12 my-3">
                <h2 class="popis">
                    Naše pohybové centrum bolo založené v roku 2020 so sídlom v Smižanoch, ktoré ponúka rôzne služby ako
                    napríklad
                    masáže, diagnostiky, fyzioterapiu, kurzy, skupinové a individuálne tréningy,konzultácie s odborníkmi
                    alebo
                    výživové poradenstvo. Aktuálne zamestnávame 9 zamestnancov a pomáhame ľuďom z okolia dať sa do pohybu,
                    alebo
                    odstrániť nepríjemné bolesti tela spôsobené prácou, či rôznymi zdravotnými obmedzeniami.
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-6 ">
                <div class="table-R" id="tabulka">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Otváracia doba</th>
                            <th>Čas</th>
                            <th>Čas</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Pondelok</th>
                            <td>7:00-12:00</td>
                            <td>13:00-19:00</td>
                        </tr>
                        <tr>
                            <th>Utorok</th>
                            <td>7:00-12:00</td>
                            <td>13:00-19:00</td>
                        </tr>
                        <tr>
                            <th>Streda</th>
                            <td>7:00-12:00</td>
                            <td>13:00-19:00</td>
                        </tr>
                        <tr>
                            <th>Štvrtok</th>
                            <td>7:00-12:00</td>
                            <td>13:00-19:00</td>
                        </tr>
                        <tr>
                            <th>Piatok</th>
                            <td>7:00-12:00</td>
                            <td>13:00-19:00</td>
                        </tr>
                        <tr>
                            <th>Sobota-Nedeľa</th>
                            <td>Zatvorené</td>
                            <td>Zatvorené</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

