<?php /** @var Array $data */?>

<div class="container mt-3">
    <h2 id="nazov_recenzia1">Uprav svoju recenziu </h2>
    <strong>Uprav svoje meno alebo recenziu </strong>
    <form>
        <div class="form-group">
            <label for="usr">Meno:</label>
            <textarea class="form-control" rows="5" id="usr" name="meno4" ><?=$data['oldName']?></textarea>
        </div>
        <div class="form-group">
            <label for="comment">Recenzia:</label>
            <textarea class="form-control" rows="5" id="comment" name="text4" ><?=$data['oldText']?></textarea>
        </div>
        <button formmethod="post" type="submit" class="btn btn-dark">Upraviť</button>
    </form>
</div>
