<?php
/** @var \App\Core\Autentifikator $auten */
$auten = new \App\Core\Autentifikator();
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="vaii/public/script.js"></script>
    <meta charset="UTF-8">
    <title>Pohybové Centrum</title>
    <link rel="stylesheet" href="vaii/public/css.css">
</head>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
        <img src="vaii/Images/logo.png" alt="logo">
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
<?php
if ($auten->JeNiektoPrihlaseny() == 0) { ?>
            <!-- Links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="?c=home&a=index">Domov</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=home&a=O_Nas">O nás</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=home&a=Kontakt">Kontakt</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=home&a=Recenzie">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=login&a=Registracia">Registrácia</a>
                </li>
                <li>
                <a href="?c=login&a=index" id="buttonNavbar" class="btn btn-warning" > Prihlásiť sa </a>
                </li>
            </ul>
        </div>
    </nav>
<?php } else if ($auten->JeToAdmin() == 1) { ?>

            <!-- Links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="?c=login&a=OsobnyUcet">Používatelia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=login&a=ZobrazOtazky">Otázky</a>
                </li>
                <li>
                <a href="?c=login&a=Odhlasenie" id="buttonNavbar" class="btn btn-warning" > Odhlásiť sa </a>
                </li>
            </ul>
        </div>
    </nav>
<?php } else { ?>
            <!-- Links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="?c=login&a=OsobnyUcet">Moje údaje</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?c=login&a=PolozOtazku">Otázky?</a>
                </li>
                <li>
                <a href="?c=login&a=Odhlasenie" id="buttonNavbar" class="btn btn-warning" > Odhlásiť sa </a>
                </li>
            </ul>
        </div>
    </nav>
<?php } ?>
<div class="web-content">
    <?= $contentHTML ?>
</div>
</html>




