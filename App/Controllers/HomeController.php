<?php

namespace App\Controllers;

use App\Core\AControllerBase;
use App\Models\DBRecenzie;
use App\Models\Recenzia;

class HomeController extends AControllerBase
{

    public function index()
    {

    }

    public function O_Nas()
    {


    }

    public function Kontakt()
    {


    }

    public function Recenzie()
    {
        $storage = new DBRecenzie();
        return [
            'recenzia' => $storage->nacitajVsetko()
        ];
    }

    public function zmazRecenziu()
    {
        $id = ($_GET["id"]);
        $storage = new DBRecenzie();
        $storage->Vymaz($id);
        header("Location: ?c=home&a=Recenzie");
        exit();
    }

    public function pridajRecenziu()
    {
        if (isset($_POST["meno3"])) {
            if ($_POST["meno3"] == "") { ?>
                <div class="container">
                    <div class="alert alert-danger">
                        <strong>Pozor!</strong> Meno musí byť vyplnené!
                    </div>
                </div>
            <?php }
            if (isset($_POST["text3"])) {
                if ($_POST["text3"] == "") { ?>
                    <div class="container">
                        <div class="alert alert-danger">
                            <strong>Pozor!</strong> Nenapísali ste nám žiadnu recenziu!
                        </div>
                    </div>
                <?php }
                if (($_POST["meno3"]) != "" && ($_POST["text3"]) != "") {
                    $storage = new DBRecenzie();
                    $recenzia = new Recenzia($_POST["meno3"], $_POST["text3"]);
                    $storage->Uloz($recenzia);
                    header("Location: ?c=home&a=Recenzie");
                }
            }
        }
    }

    public function upravRecenziu()
    {
        $storage = new DBRecenzie();

        if (isset($_POST["meno4"])) {
            if ($_POST["meno4"] == "") { ?>
                <div class="container">
                    <div class="alert alert-danger">
                        <strong>Pozor!</strong> Meno musí byť vyplnené!
                    </div>
                </div>
            <?php }
            if (isset($_POST["text4"])) {
                if ($_POST["text4"] == "") { ?>
                    <div class="container">
                        <div class="alert alert-danger">
                            <strong>Pozor!</strong> Nenapísali ste nám žiadnu recenziu!
                        </div>
                    </div>
                <?php }
                if (($_POST["meno4"]) != "" && ($_POST["text4"]) != "") {
                    $recenzia = new Recenzia($_POST["meno4"], $_POST["text4"]);
                    $storage->Uprav($_GET["id"], $recenzia->getMeno(), $recenzia->getText());
                    header("Location: ?c=home&a=Recenzie");
                }
            }
        }
        return [
            'oldName' => $_POST["stareMeno"],
            'oldText' => $_POST["staryText"]
        ];
    }
}