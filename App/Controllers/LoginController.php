<?php

namespace App\Controllers;

use App\Core\AControllerBase;
use App\Models\DBOtazky;
use App\Models\Otazka;
use App\Models\DBPouzivatelia;
use App\Models\Pouzivatel;
use App\Core\Autentifikator;

class LoginController extends AControllerBase
{
    public function index()
    {
        $storage = new DBPouzivatelia();
        if (isset($_POST["submit"]) && ($storage->JeToUzivatel($_POST["email"], $_POST["password"]) == 1)) {
            $this->prihlas($storage->DajUzivatela($_POST["email"]));
            header("Location: ?c=login&a=OsobnyUcet");
        }

        if(isset($_POST["submit"]) && ($storage->JeToUzivatel($_POST["email"],$_POST["password"]) == 0)) {
            return [
                'message' => "Zadali ste nesprávne heslo! Skúste to znovu.",
                'clas' => "alert alert-danger",
                'role' => "alert"
            ];
        }
        return [
            'message' => "",
            'clas' => "alert alert-white",
            'role' => "alert"
        ];
    }

    public function registracia()
    {
        if (isset($_POST["meno1"]) || isset($_POST["priezvisko1"]) || isset($_POST["email1"]) || isset($_POST["heslo1"])) {
            if ($_POST["meno1"] == "" || $_POST["priezvisko1"] == "" || $_POST["email1"] == "" || $_POST["heslo1"] == "") { ?>
                <div class="container">
                    <div class="alert alert-danger">
                        <strong>Pozor!</strong> Všetky polia musia byť vyplnené!
                    </div>
                </div>
            <?php }

            if (($_POST["meno1"]) != "" && ($_POST["priezvisko1"]) != "" && ($_POST["email1"]) != "" && ($_POST["heslo1"]) != "") {
                $storage = new DBPouzivatelia();
                $pouzivatel1 = new Pouzivatel($_POST["meno1"], $_POST["priezvisko1"], $_POST["email1"], $_POST["heslo1"]);
                $storage->UlozKlienta($pouzivatel1);
                header("Location: ?c=login&a=index");
            }
        }

    }

    public function odhlasenie()
    {
        $auten = new Autentifikator();
        $auten->Odhlasenie();
    }

    public function prihlas(Pouzivatel $uzivatel) {
        $auten = new Autentifikator();
        $auten->Prihlasenie($uzivatel);
        header("Location: ?c=login&a=OsobnyUcet");
    }


    public function PolozOtazku()
    {
        if (isset($_POST["text3"])) {
            if ($_POST["text3"] == "") {
                return [
                'message' => "Pozor! Nenapísali ste nám žiadnu otázku!",
                'clas' => "alert alert-danger",
                'role' => "alert"
                ];
            }
            if (($_POST["text3"]) != "") {
                $storage = new DBOtazky();
                $otazka = new Otazka($_POST["text3"]);
                $storage->UlozOtazku($otazka);
                header("Location: ?c=login&a=OsobnyUcet");
            }
        }
        return [
            'message' => "",
            'clas' => "alert alert-white",
            'role' => "alert"
        ];
    }

    public function ZobrazOtazky() {
        $storage = new DBOtazky();
        return [
            'otazky' => $storage->nacitajVsetkyOtazky()
        ];
    }

    public function zmazOtazku()
    {
        $id = ($_GET["id"]);
        $storage = new DBOtazky();
        $storage->VymazOtazku($id);
        header("Location: ?c=login&a=ZobrazOtazky");
        exit();
    }

    public function OsobnyUcet() {
        $auten = new Autentifikator();
        if ($auten->JeNiektoPrihlaseny() == 0) {
            header("Location: ?c=login&a=index");
        }
        if($auten->JeToAdmin() == 1 ) {
            $storage = new DBPouzivatelia();
            return [
                'users' => $storage->nacitajVsetko()
            ];
        } else {

        }
    }

    public function VymazKlienta() {
        if (isset($_GET["id"])) {
            $uzivatelia = new DBPouzivatelia();
            $uzivatelia->VymazKlienta($_GET["id"]);
            unset($_POST["id"]);
            header("Location: ?c=login&a=OsobnyUcet");
        }
    }
}