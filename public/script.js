
var x, pouzivatelia, xmlDoc
pouzivatelia = new XMLHttpRequest();
pouzivatelia.open("GET", "vaii/App/Core/ajaxPouzivatelia.php", true);
pouzivatelia.send();
xmlDoc = pouzivatelia.responseXML;
pouzivatelia.onreadystatechange = function()
{
    var x = JSON.parse(this.responseText)
    var table = "<thead>\n" +
        "        <tr>\n" +
        "            <th>ID</th>\n" +
        "            <th>MENO</th>\n" +
        "            <th>PRIEZVISKO</th>\n" +
        "            <th>EMAIL</th>\n" +
        "            <th>HESLO</th>\n" +
        "            <th></th>\n" +
        "        </tr>\n" +
        "    </thead> ";
    for (var i = 0; i < x.length; i++) {

        var id = x[i].id;
        var meno = x[i].meno;
        var priezvisko = x[i].priezvisko;
        var mail = x[i].mail;

        table += "<tr>";
        table += "<td>" + id + "</td>";
        table += "<td>" + meno + "</td>";
        table += "<td>" + priezvisko + "</td>";
        table += "<td>" + mail + "</td>";
        table += "<td>" + "*****" + "</td>";
        table += '<td><a href="?c=login&a=VymazKlienta&id=' + id + ' " class="btn btn-warning ">Zmaž</a></td>';
        table += "<tr>";
    }
    document.getElementById("tableUsers").innerHTML = table;
}

var y, otazky, doc
otazky = new XMLHttpRequest();
otazky.open("GET", "vaii/App/Core/ajaxOtazky.php", true);
otazky.send();
doc = pouzivatelia.responseXML;
otazky.onreadystatechange = function()
{
    var y = JSON.parse(this.responseText)
    var table = "<thead>\n" +
        "        <tr>\n" +
        "            <th>ID</th>\n" +
        "            <th>OTÁZKA</th>\n" +
        "            <th></th>\n" +
        "        </tr>\n" +
        "    </thead> ";
    for (var i = 0; i < y.length; i++) {
        var id = y[i].id;
        var otazka = y[i].text;


        table += "<tr>";
        table += "<td>" + id + "</td>";
        table += "<td>" + otazka + "</td>";
        table += '<td><a href="?c=login&a=zmazOtazku&id=' + id + ' " class="btn btn-warning ">Zmaž</a></td>';
        table += "<tr>";
    }
    document.getElementById("tableOtazky").innerHTML = table;
}
